# nginx-hosts-docker
I was experimenting with how to configure nginx to serve different content for different hostnames in the request. Assuming that the nginx server is reachable by each of those DNS addresses.

## How to run?
First configure your etc hosts to add the following records (/etc/hosts on linux, if on Windows or Mac OS X then feel free to Google wherever it is)
```
127.0.0.1	localhost thor epic.thor cool.thor aspnet.thor
```
And then run with docker-compose up. Once up and running open http://localhost, http://thor, http://epic.thor, http://cool.thor, http://aspnet.thor. The last URL with serve you the reverse proxied ASP.NET Core Sample site provided by the microsoft image. Really useful to be able to set up multiple sites on your dev machine and reach them all on port 80 with a logical naming structure!
